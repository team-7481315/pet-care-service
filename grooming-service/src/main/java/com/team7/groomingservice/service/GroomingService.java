package com.team7.groomingservice.service;

import com.team7.groomingservice.model.Grooming;
import com.team7.groomingservice.repository.GroomingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroomingService {
    @Autowired
    private GroomingRepository groomingRepository;

    public Grooming saveGrooming(Grooming grooming){
        return groomingRepository.save(grooming);
    }

    public List<Grooming> getAllGroomings(){
        return groomingRepository.findAll();
    }

    public Grooming getGroomingByID(Long id){
        return groomingRepository.findById(id).orElse(null);
    }
}
