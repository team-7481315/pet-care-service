package com.team7.groomingservice.controller;

import com.team7.groomingservice.model.Grooming;
import com.team7.groomingservice.service.GroomingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/grooming")
public class GroomingController {
    @Autowired
    private GroomingService groomingService;
    @GetMapping("/all")
    public ResponseEntity<List<Grooming>> getAllGrooming(){
        List<Grooming> list = groomingService.getAllGroomings();
        return new ResponseEntity<List<Grooming>>(list, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Grooming> createGrooming(@RequestBody Grooming grooming) {
        Grooming newGrooming = groomingService.saveGrooming(grooming);
        return new ResponseEntity<>(newGrooming, HttpStatus.CREATED);
    }

    @GetMapping("/getOneGroomingById/{id}")
    public ResponseEntity<Grooming> getOneById(@PathVariable Long id){
        Grooming grooming = groomingService.getGroomingByID(id);
        return new ResponseEntity<>(grooming, HttpStatus.OK);
    }

//    @GetMapping("/retry")
//    public ResponseEntity<Grooming> getOneById(@PathVariable Long id){
//        Grooming grooming = groomingService.getGroomingByID(id);
//        return new ResponseEntity<>(grooming, HttpStatus.OK);
//    }
}
