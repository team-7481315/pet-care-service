package com.team7.groomingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroomingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroomingServiceApplication.class, args);
	}

}
