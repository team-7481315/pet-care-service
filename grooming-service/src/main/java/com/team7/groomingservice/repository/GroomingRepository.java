package com.team7.groomingservice.repository;

import com.team7.groomingservice.model.Grooming;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroomingRepository extends JpaRepository<Grooming, Long> {

}
