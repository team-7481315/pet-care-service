package com.team7.favoriteservice.service;

import com.team7.favoriteservice.model.Product;
import com.team7.favoriteservice.repository.ProductReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductReponsitory productReponsitory;

    public Product saveProduct(Product product){
        return productReponsitory.save(product);
    }

    public List<Product> getAllProducts(){
        return productReponsitory.findAll();
    }

    public Product getProductById(Long id){
        return productReponsitory.findById(id).orElse(null);
    }

}
