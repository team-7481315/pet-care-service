package com.team7.orderservice.service;

import com.team7.orderservice.model.Order;
import com.team7.orderservice.repository.OrderRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class OrderService {
        private OrderRepository orderRepository;

        public OrderService(OrderRepository orderRepository){
            this.orderRepository = orderRepository;
        }

        public Order createOrder(Order order){
            return orderRepository.save(order);
        }

        public List<Order> getAllOrder(){
            return orderRepository.findAll();
        }

        public Order getOrderById(Long id){
            return orderRepository.findById(id).orElse(null);
        }

}
