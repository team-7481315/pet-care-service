package com.team7.orderservice.controller;

import com.team7.orderservice.model.Order;
import com.team7.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v1/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/all")
    public ResponseEntity<List<Order>> getAllOrder(){
        List<Order> list = orderService.getAllOrder();
        return new ResponseEntity<List<Order>>(list, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
        Order newOrder = orderService.createOrder(order);
        return new ResponseEntity<Order>(newOrder, HttpStatus.CREATED);
    }

    @GetMapping("/getOneOrderById/{id}")
    public ResponseEntity<Order> getOneById(@PathVariable Long id){
        Order order = orderService.getOrderById(id);
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }
}
