package com.team7.userservice.model;

public enum Role {
    USER, ADMIN
}
