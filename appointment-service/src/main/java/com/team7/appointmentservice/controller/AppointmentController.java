package com.team7.appointmentservice.controller;

import com.team7.appointmentservice.model.Appointment;
import com.team7.appointmentservice.service.AppointmentService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RequestMapping("/api/v1/appointment")
@Controller
public class AppointmentController {
    @Autowired
    private AppointmentService appointmentService;

    @GetMapping("/all")
    public ResponseEntity<List<Appointment>> getAllAppointment() {
        List<Appointment> list = appointmentService.getAllAppointment();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    private static final Logger logger = LoggerFactory.getLogger(AppointmentController.class);

    @PostMapping("/create")
    @Retry(name = "appointmentServiceRetry", fallbackMethod = "fallbackMethod")
    @CircuitBreaker(name = "appointmentServiceCB", fallbackMethod = "fallbackMethod")
    @RateLimiter(name = "appointmentServiceRL", fallbackMethod = "rateLimiterFallbackMethod")
    public ResponseEntity<String> createAppointment(@RequestBody Appointment appointment) {
        logger.info("Attempting to create appointment");
        appointmentService.createAppointment(appointment);
        return new ResponseEntity<>("Appointment created successfully!", HttpStatus.CREATED);
    }

    public ResponseEntity<String> fallbackMethod(Appointment appointment, Throwable throwable) {
        logger.error("Fallback method called due to: ", throwable);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Oops! Something went wrong, please try again later!");
    }

    public ResponseEntity<String> rateLimiterFallbackMethod(Appointment appointment, Throwable throwable) {
        logger.warn("Rate limiter fallback method called due to: ", throwable);
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).body("Too many requests, please try again later.");
    }

    @GetMapping("/getOneAppointmentById/{id}")
    public ResponseEntity<Appointment> getOneById(@PathVariable Long id){
        Appointment appointment = appointmentService.getAppointmentById(id);
        if(appointment == null){
            logger.info("Không có giá trị");
        } else  logger.info("Có giá trị");

        return new ResponseEntity<>(appointment, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Appointment> updateAppointment(@PathVariable Long id, @RequestBody Appointment updatedAppointment) {
        Appointment appointment = appointmentService.updateAppointment(id, updatedAppointment);
        if (appointment != null) {
            return new ResponseEntity<>(appointment, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAppointment(@PathVariable Long id) {
        appointmentService.deleteAppointment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}


