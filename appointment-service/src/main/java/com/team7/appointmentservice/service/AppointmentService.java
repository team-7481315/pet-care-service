package com.team7.appointmentservice.service;

import com.team7.appointmentservice.dto.AppointmentResponse;
import com.team7.appointmentservice.model.Appointment;
import com.team7.appointmentservice.repository.AppointmentRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AppointmentService {
    private final AppointmentRepository appointmentRepository;
    private final RestTemplate restTemplate;

    @Autowired
    private RedisTemplate<String, Appointment> redisTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentService.class);

    public AppointmentService(AppointmentRepository appointmentRepository, RestTemplate restTemplate, RedisTemplate<String, Appointment> redisTemplate) {
        this.appointmentRepository = appointmentRepository;
        this.restTemplate = restTemplate;
        this.redisTemplate = redisTemplate;
    }

//    private AppointmentDetail mapToDto(AppointmentDtl item) {
//        // TODO Auto-generated method stub
//        AppointmentDetail o = new AppointmentDetail(item.getGroomingName(), item.getGroomingId(), item.getPrice(), item.getQuatity());
//        return o;
//    }
//
    public Appointment createAppointment(Appointment appointment){
        try {
            return appointmentRepository.save(appointment);
        } catch (Exception e) {
            throw new RuntimeException("Error creating appointment", e); // Ném ra RuntimeException khi có lỗi
        }
        // Tạm thời ném ra RuntimeException để kiểm tra Retry và CircuitBreaker
//        throw new RuntimeException("Simulated service failure");
    }

    public List<Appointment> getAllAppointment() {
        String key = "allAppointments";
        List<Appointment> appointments;

        // Kiểm tra xem dữ liệu có trong Redis không
        Boolean hasKey = redisTemplate.hasKey(key);
        if (Boolean.TRUE.equals(hasKey)) {
            // Nếu có, lấy dữ liệu từ Redis
            appointments = redisTemplate.opsForList().range(key, 0, -1);
            LOGGER.info("Data retrieved from Redis");
        } else {
            // Nếu không, truy vấn cơ sở dữ liệu
            appointments = appointmentRepository.findAll();
            LOGGER.info("Data retrieved from database");

            // Lưu dữ liệu vào Redis
            for (Appointment appointment : appointments) {
                redisTemplate.opsForList().rightPush(key, appointment);
            }
        }

        return appointments;
    }

    public Appointment getAppointmentById(Long id){
        return appointmentRepository.findById(id).orElse(null);
    }

    public void deleteAppointment(Long id) {
        try {
            appointmentRepository.deleteById(id);
            LOGGER.info("Appointment with ID {} deleted successfully", id);

            // Lấy cuộc hẹn từ cơ sở dữ liệu
            Optional<Appointment> appointmentOptional = appointmentRepository.findById(id);
            if (appointmentOptional.isPresent()) {
                Appointment appointmentToDelete = appointmentOptional.get();

                // Xóa cuộc hẹn khỏi Redis nếu tồn tại
                String key = "allAppointments";
                if (redisTemplate.opsForList().remove(key, 1, appointmentToDelete) > 0) {
                    LOGGER.info("Appointment with ID {} removed from Redis", id);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error deleting appointment with ID {}", id, e);
            throw new RuntimeException("Error deleting appointment", e);
        }
    }

    public Appointment updateAppointment(Long id, Appointment updatedAppointment) {
        Optional<Appointment> optionalAppointment = appointmentRepository.findById(id);
        if (optionalAppointment.isPresent()) {
            Appointment existingAppointment = optionalAppointment.get();

            // Cập nhật các trường cần thiết từ updatedAppointment
            if (updatedAppointment.getAppointName() != null) {
                existingAppointment.setAppointName(updatedAppointment.getAppointName());
            }
            if (updatedAppointment.getCustomerName() != null) {
                existingAppointment.setCustomerName(updatedAppointment.getCustomerName());
            }
            if (updatedAppointment.getCustomerEmail() != null) {
                existingAppointment.setCustomerEmail(updatedAppointment.getCustomerEmail());
            }
            if (updatedAppointment.getCustomerAddress() != null) {
                existingAppointment.setCustomerAddress(updatedAppointment.getCustomerAddress());
            }
            if (updatedAppointment.getType() != null) {
                existingAppointment.setType(updatedAppointment.getType());
            }
            if (updatedAppointment.getPrice() != null) {
                existingAppointment.setPrice(updatedAppointment.getPrice());
            }
            // Lưu lại ngày tạo ban đầu
            existingAppointment.setCreateDate(existingAppointment.getCreateDate());

            try {
                return appointmentRepository.save(existingAppointment);
            } catch (Exception e) {
                LOGGER.error("Error updating appointment with ID {}", id, e);
                throw new RuntimeException("Error updating appointment", e);
            }
        } else {
            LOGGER.error("Appointment with ID {} not found", id);
            throw new RuntimeException("Appointment not found");
        }
    }





}

