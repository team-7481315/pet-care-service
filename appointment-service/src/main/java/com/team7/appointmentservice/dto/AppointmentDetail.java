//package com.team7.appointmentservice.dto;
//
//import com.team7.appointmentservice.model.Appointment;
//import com.team7.groomingservice.model.Grooming;
//import jakarta.persistence.*;
//
//import java.util.List;
//
//public class AppointmentDetail {
//
//    private Appointment appointment;
//    private List<Grooming> grooming;
//
//    public AppointmentDetail(Appointment appointment, List<Grooming> grooming) {
//        this.appointment = appointment;
//        this.grooming = grooming;
//    }
//
//    public AppointmentDetail() {
//    }
//
//    public Appointment getAppointment() {
//        return appointment;
//    }
//
//    public void setAppointment(Appointment appointment) {
//        this.appointment = appointment;
//    }
//
//    public List<Grooming> getGrooming() {
//        return grooming;
//    }
//
//    public void setGrooming(List<Grooming> grooming) {
//        this.grooming = grooming;
//    }
//}
