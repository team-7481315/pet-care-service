package com.team7.appointmentservice.dto;

import java.util.List;

public class AppointmentRequest {
    private String customerEmail;

    private String customerAddress;

    private List<AppointmentResponse> appointmentResponseList;

    public AppointmentRequest(List<AppointmentResponse> appointmentResponseList) {
        this.appointmentResponseList = appointmentResponseList;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public List<AppointmentResponse> getAppointmentDtlList() {
        return appointmentResponseList;
    }

    public void setAppointmentDtlList(List<AppointmentResponse> appointmentResponseList) {
        this.appointmentResponseList = appointmentResponseList;
    }
}
