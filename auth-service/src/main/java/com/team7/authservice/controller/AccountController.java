package com.team7.authservice.controller;

import com.team7.authservice.model.Account;
import com.team7.authservice.model.LoginRequestDto;
import com.team7.authservice.model.RegisterRequestDto;
import com.team7.authservice.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequestDto registerRequestDto) throws Exception {
        return new ResponseEntity<>(accountService.register(registerRequestDto),HttpStatus.OK);
    }


    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginRequestDto loginRequestDto) {
        return new ResponseEntity<>(accountService.login(loginRequestDto),HttpStatus.OK);
    }

    @GetMapping("/all")
    public List<Account> getAllAccount(){
        return accountService.getAllAccount();
    }
}
