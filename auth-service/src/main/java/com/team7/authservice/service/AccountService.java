package com.team7.authservice.service;

import com.team7.authservice.model.Account;
import com.team7.authservice.model.AuthResponseDto;
import com.team7.authservice.model.LoginRequestDto;
import com.team7.authservice.model.RegisterRequestDto;
import com.team7.authservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import lombok.RequiredArgsConstructor;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

  private final  UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  private final  JwtService jwtService;

  private final AuthenticationManager authenticationManager;

  public AuthResponseDto login(LoginRequestDto request) {

    var user = userRepository.findByUsername(request.getUsername()).orElseThrow();
    var jwt = jwtService.generateToken(user);
    return new AuthResponseDto(jwt);
  }

  public Account register(RegisterRequestDto  request) {
      if (userRepository.existsByUsername(request.getUsername())) {
          throw new IllegalArgumentException("Username already exists");
      }
   Account account = new Account();
   account.setUsername(request.getUsername());
   account.setPassword(passwordEncoder.encode(request.getPassword()));
      return userRepository.save(account);
  }
  public List<Account> getAllAccount(){
    return userRepository.findAll();
  }
    public Account getAccount(String username){
        return userRepository.findByUsername(username).orElseThrow();
    }
}
